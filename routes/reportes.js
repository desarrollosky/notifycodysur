const { isLoggedIn } = require('../config/auth');
const connection = require('../config/db');

module.exports = function(express, globals) {
    const router = express.Router();
    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Reportes',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('reportes/index', content);
        }
    };
    router.get('/index', isLoggedIn, routes.index);

    return router;
};

module.exports.estadisticas_grupos = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {};

    connection.query("SELECT count(*) as urgente from notificacion WHERE estado=1; SELECT count(*) as proceso from notificacion WHERE estado=2; SELECT count(*) as concluido from notificacion WHERE estado=3;", function(err, rows, fields) {
        if (err) { throw err; } else {

            data = {
                urgente: rows[0],
                proceso: rows[1],
                concluido: rows[2]
            }
            res.json(data);
        }
    });
}

module.exports.graficas_barra = function(req, res) {

    /*2. Botón SOS~
    3. Desconexión de energía~
    4. Perdida de posición por 15 mins~
    1. Detección de jammer~
    15. Parada NO autorizada 10 min~

    12. Detención por 5 min en ruta~*/
    

    res.setHeader('Content-type', 'text/plain');
    let inicio = req.body.inicio;
    let final = req.body.final;
    var sql = "";
    var data = {
        "perdidaPos": "",
        "jammer": "",
        "pos10min": "",
        //"combust": "",
        //"exvel": "",
        //"carga": "",
        "sos": "",
        "desconect": "",
        "detencion": ""
    };

    if ((inicio === undefined || inicio === null || inicio === "") && (final === undefined || final === null || final === "")) {
        //Perdida de posicion
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='4. Perdida de posición por 15 mins ~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Jammer
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='1. Detección de jammer ~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Parada prohibida (Parada no autorizada)
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='15. Parada NO autorizada 10 min ~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Descarga de combustible
        // sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE (nombre='5. Descarga de combustible~' OR nombre='5. Descarga de combustible~') GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Exceso de velocidad
        // sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE (nombre='1. Exceso de velocidad 90 km/h~' OR nombre='1. Exceso de velocidad 120 km/h~' OR nombre='6. Exceso de Velocidad 100 km/h') GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Recarga de combustible
        // sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE (nombre='4. Recarga de combustible~' OR nombre='7. Recarga de Combustible') GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Presiones SOS
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='2. Botón SOS ~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Desconexión de energía
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='3. Desconexión de energía ~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='Detención por 5 min en ruta ~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";

        connection.query(sql, function(err, rows, fields) {
            if (err) { throw err; } else {
                data["perdidaPos"] = rows[0];
                data["jammer"] = rows[1];
                data["pos10min"] = rows[2];
                //data["combust"] = rows[3];
                //data["exvel"] = rows[4];
                //data["carga"] = rows[5];
                data["sos"] = rows[3];
                data["desconect"] = rows[4];
                data["detencion"] = rows[5];
                res.json(data);
            }
        });
    } else {
        inicio = inicio + " 00:00:00";
        final = final + " 23:59:59";

        //Perdida de posicion
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='4. Perdida de posición por 15 mins ~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Jammer
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='1. Detección de jammer ~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Parada prohibida
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='15. Parada NO autorizada 10 min ~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Descarga de combustible
        // sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE (nombre='5. Descarga de combustible~' OR nombre='5. Descarga de combustible~') AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Exceso de velocidad
        // sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE (nombre='1. Exceso de velocidad 90 km/h~' OR nombre='1. Exceso de velocidad 120 km/h~' OR nombre='6. Exceso de Velocidad 100 km/h') AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Recarga de combustible
        // sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE (nombre='4. Recarga de combustible~' OR nombre='7. Recarga de Combustible') AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Presiones SOS
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='2. Botón SOS ~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Desconexión de energía
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE (nombre='3. Desconexión de energía ~' OR nombre='Desconexión de energía~') AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='Detención por 5 min en ruta ~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";

        connection.query(sql, [inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final], function(err, rows, fields) {
            if (err) { throw err; } else {
                data["perdidaPos"] = rows[0];
                data["jammer"] = rows[1];
                data["pos10min"] = rows[2];
                //data["combust"] = rows[3];
                //data["exvel"] = rows[4];
                //data["carga"] = rows[5];
                data["sos"] = rows[3];
                data["desconect"] = rows[4];
                data["detencion"] = rows[5];
                res.json(data);
            }
        });
    }
}

module.exports.promedio_cierre = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    var sql = "";
    var data = {
        "cierre": "",
    };

    sql += "SELECT COUNT(*) totalN, ROUND(((SELECT COUNT(estado) concluidas FROM notificacion WHERE estado='3')/(SELECT COUNT(*) totalnotif from notificacion) * 100),2) AS promedioCierre FROM notificacion;";

    connection.query(sql, function(err, rows, fields) {
        if (err) { throw err; } else {
            data["cierre"] = rows[0];
            res.json(data);
        }
    });
}

module.exports.promedio_asistencia = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    var sql = "";
    var data = {
        "asistencia": "",
    };

    sql += "SELECT COUNT(*) totalN, ROUND(((SELECT COUNT(DISTINCT(idNotificacion)) asistencia FROM comentario)/(SELECT COUNT(*) totalnotif from notificacion) * 100),2) AS promedioAsist FROM notificacion";

    connection.query(sql, function(err, rows, fields) {
        if (err) { throw err; } else {
            data["asistencia"] = rows[0];
            res.json(data);
        }
    });
}